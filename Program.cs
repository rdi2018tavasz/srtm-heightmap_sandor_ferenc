﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace hazi1
{
    class Program
    {
        static void Main(string[] args)
        {
            //AlkalmazasNeve.exe input output ElevationTreshold
            
            HeightMap HG = HeightMap.Parse(args[0]);
            HG.ElevationThreshold = int.Parse(args[2]);
            HG.SaveToBitmap(args[1]);
            
        }
    }
}
