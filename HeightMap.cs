﻿using System;
using System.Text;
using System.IO;
using System.Drawing;

namespace hazi1
{
    
    class HeightMap
    {
        const int SIZE = 1201;
        int[,] HeightData;
        int elevationThreshold;
        public int ElevationThreshold {
            get
            {
                return elevationThreshold;
            }
            set
            {
                this.elevationThreshold = value;
            }
        }

        public HeightMap()
        {
            this.HeightData = new int[SIZE, SIZE];
        }

        public static HeightMap Parse(string path){
            HeightMap Hg = new HeightMap();
            FileStream fs;
            int[] buffer = new int[2];
            using (fs = new FileStream(path, FileMode.Open))
            {
                using(BinaryReader br = new BinaryReader(fs, Encoding.BigEndianUnicode))
                {

                    for(int i = 0; i < SIZE; i++)
                    {
                        for (int j = 0; j < SIZE; j++)
                        {
                            
                            buffer[0] = br.ReadByte();
                            buffer[1] = br.ReadByte();
                            Hg.HeightData[i, j] = buffer[0] << 8 | buffer[1];
                           
                            if(Hg.HeightData[i,j] == 32768)
                                Hg.HeightData[i, j] = 0;
                            
                        }
                    }
                }
            }
                return Hg;
        }

        private int[] GetExtremalElevation(){
            int[] result = new int[2];
            int min = int.MaxValue;
            int max = int.MinValue;

            for (int i = 0; i < this.HeightData.GetLength(0); i++)
            {
                for (int j = 0; j < this.HeightData.GetLength(1); j++)
                {
                    if (this.HeightData[i, j] < min)
                        min = this.HeightData[i, j];
                    if (this.HeightData[i, j] > max)
                        max = this.HeightData[i, j];
                }
            }

            result[0] = min;
            result[1] = max;

            return result;
        }

        public void SaveToBitmap(string path) {
            Bitmap bmp = new Bitmap(SIZE, SIZE);
            Color c = Color.FromArgb(0,0,0);

            int[] extremal = this.GetExtremalElevation();
            float interval = (float)(extremal[1] - extremal[0]);
            float divider = interval / 255;

            for (int i = 0; i < SIZE; i++)
            {
                for(int j = 0; j < SIZE; j++)
                {
                    if (HeightData[i, j] > this.ElevationThreshold)
                    {
                        c = Color.FromArgb(0, (int)(255 - (HeightData[i,j]/divider)), 0);
                    }
                    else
                        c = Color.FromArgb(0, 0, 255);

                    bmp.SetPixel(j,i,c);
                }
                
            }
            bmp.Save(path);
        }
        
    }
}
